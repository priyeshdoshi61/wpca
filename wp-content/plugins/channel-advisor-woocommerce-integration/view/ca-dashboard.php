<input type="hidden" value="true" id="ca_dashboard_page" />
<div class="loading" style="display: none">Loading&#8230;</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <div class="col-md-9">
                    <div class="wrap">
                        <h1 class="wp-heading-inline">Channel Advisor Wordpress Integration</h1>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <p></p>
            <table class="wp-list-table widefat fixed striped table-striped">
                <tbody>
                <!--<tr class="alternate">
                    <td class="column-columnname">Push products to Channel Advisor.</td>
                    <td class="column-columnname">
                        <button class="button button-primary" id="push_products">Push Products</button>
                        <img style="display:none;" src="<?/*= CA_BASE_URL.'images/ajax-loader.gif' */?>" id="push_product_loader">
                    </td>
                </tr>-->
                <tr class="alternate">
                    <td class="column-columnname">Update stocks at Channel Advisor.</td>
                    <td class="column-columnname">
                        <button class="button button-primary" id="update_stock">Update Product Stock</button>
                    </td>
                </tr>
                <tr class="alternate">
                    <td class="column-columnname">Import orders from Channel Advisor.</td>
                    <td class="column-columnname">
                        <button class="button button-primary" id="import_orders">Import orders</button>
                    </td>
                </tr>
                <tr class="alternate">
                    <td class="column-columnname">Update orders status to Channel Advisor.</td>
                    <td class="column-columnname">
                        <button class="button button-primary" id="update_order_status">Update orders status</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  style="margin-top: 20%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="modalContent">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->