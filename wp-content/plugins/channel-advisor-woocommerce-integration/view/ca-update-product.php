<input type="hidden" value="true" id="ca_product_update_page" />
<div class="loading" style="display: none">Loading&#8230;</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="wp-heading-inline">Update Products</h1>
            <table class="widefat fixed striped" id="ca_product_update">
                <thead>
                <tr>
                    <th>Sku</th>
                    <th>Product name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($channelAdvisorSku as $sku=>$product) { ?>
                        <tr>
                            <?php if(!is_null($product['WordpressId'])) { ?>
                                <td><a href="<?= get_edit_post_link($product['WordpressId']) ?>" target="_blank"><?= $product['Sku']; ?></a></td>
                            <?php } else { ?>
                                <td><?= $product['Sku']; ?></td>
                            <?php } ?>
                            <?php if(!is_null($product['WordpressId'])) { ?>
                                <td><a href="<?= get_permalink($product['WordpressId']) ?>" target="_blank"> <?= $product['Title']; ?></a></td>
                            <?php } else { ?>
                                <td><?= $product['Title']; ?></td>
                            <?php } ?>
                            <td><button id="<?= $sku; ?>" class="button button-primary ca_update_product">Update</button></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  style="margin-top: 20%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="modalContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->