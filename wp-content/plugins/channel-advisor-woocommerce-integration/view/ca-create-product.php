<input type="hidden" value="true" id="ca_product_management_page" />
<div class="loading" style="display: none">Loading&#8230;</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="wp-heading-inline">Create Products</h1>
            <table class="widefat fixed striped" id="ca_product_details">
                <thead>
                <tr>
                    <th><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                    <th>ID</th>
                    <th>Sku</th>
                    <th>Product name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($wordpressFormattedSku as $sku=>$product) { ?>
                        <?php if(array_key_exists($sku, $products_common_in_wp_ca)) { ?>
                            <tr>
                                <td></td>
                                <td><?= $product['WordpressId']; ?></td>
                                <td>
                                    <a href="<?= get_edit_post_link($product['WordpressId']) ?>" target="_blank"><?= $product['Sku']; ?></a>
                                </td>
                                <td>
                                    <a href="<?= get_permalink($product['WordpressId']) ?>" target="_blank"> <?= $product['Title']; ?></a>
                                </td>
                                <td>Product Exists</td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td></td>
                                <td><?= $product['WordpressId']; ?></td>
                                <td>
                                    <a href="<?= get_edit_post_link($product['WordpressId']) ?>" target="_blank"><?= $product['Sku']; ?></a>
                                </td>
                                <td>
                                    <a href="<?= get_permalink($product['WordpressId']) ?>" target="_blank"><?= $product['Title']; ?></a>
                                </td>
                                <td id="td_create_<?= $product['WordpressId']; ?>"><button id="<?= $product['WordpressId']; ?>" class="button button-primary ca_create_product">Create</button></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    <?php foreach ($products_only_present_in_ca as $sku=>$product) { ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><?= $product['Sku']; ?></td>
                            <td><?= $product['Title']; ?></td>
                            <td><button id="<?= $sku; ?>" class="button button-primary ca_delete_product">Delete</button></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <button class="button button-primary ca_create_selected_product" style="float: right; display: none">Create Selected</button>
</div>
<!-- Modal -->
<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 20%">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="modalContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->