jQuery(document).ready( function($) {
    var product_management_page = $("#ca_product_management_page").val();
    var product_update_page = $("#ca_product_update_page").val();

    if(product_management_page == "true"){
        var table = $('#ca_product_details').DataTable({
            "order": [[4, "asc"]],
            columnDefs: [
                {
                    orderable: false,
                    targets:   0,
                    searchable:false,
                    render: function (data, type, full, meta){
                        if(full[4].indexOf("Create") != -1){
                            return '<input type="checkbox" name="id[]" value="'
                                + full[1] + '">';
                        } else {
                            return "";
                        }
                    }
                },
                {"width": "7%", "targets": 1},
                { "width": "25%", "targets": 2 },
                { "width": "50%", "targets": 3 },
                { "width": "13%", "targets": 4 }
            ]
        });

        // Handle click on "Select all" control
        $('#example-select-all').on('click', function(){
            // Check/uncheck all checkboxes in the table
            var rows = table.rows({ 'search': 'applied' }).nodes();
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
            if(this.checked){
                $('.ca_create_selected_product').css('display', 'block');
                $('.ca_create_product').prop('disabled','true');
            } else {
                $('.ca_create_selected_product').css('display', 'none');
                $('.ca_create_product').removeAttr('disabled');
            }
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#ca_product_details tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is unchecked
            if(!this.checked){
                var length = $('input[type="checkbox"]:checked').length;
                if(length > 0){
                    $('.ca_create_selected_product').css('display', 'block');
                    $('.ca_create_product').prop('disabled','true');
                } else {
                    $('.ca_create_selected_product').css('display', 'none');
                    $('.ca_create_product').removeAttr('disabled');
                }
            } else {
                $('.ca_create_selected_product').css('display', 'block');
                $('.ca_create_product').prop('disabled','true');
            }
        });

        //Create multiple products
        $('.ca_create_selected_product').on('click', function(){
            $('.loading').css('display', 'block');
            //$('.ca_create_selected_product').attr('disabled','disabled');
            var productId = [];
            table.$('input[type="checkbox"]:checked').each(function(){
                productId.push(this.value);
            });
            var data = {
                action: 'ca_create_multiple_products',
                productId: JSON.stringify(productId)
            };
            $.post(the_ajax_script.ajaxurl, data, function(response) {
                $('.loading').css('display', 'none');
                $("#modalContent").html(response);
                $("#notificationModal").modal("show");
            });
        });

        /*
        * Delete a single product from wordpress to channelAdvisor
        * */
        jQuery(document).on('click','.ca_delete_product',function(){
            var sku = this.id;
            $('.loading').css('display', 'block');
            var data = {
                action: 'ca_delete_product',
                post_var: sku
            };
            $.post(the_ajax_script.ajaxurl, data, function(response) {
                $('.loading').css('display', 'none');
                $("#modalContent").html("Product deleted Successfully");
                $("#notificationModal").modal("show");
                table
                    .row( $('#'+sku).parents('tr') )
                    .remove()
                    .draw();
            });
            return false;
        });
    }

    if(product_update_page == "true"){
        var updaye_table = $('#ca_product_update').DataTable();
    }

    /*
    * Create a single product from wordpress to channelAdvisor
    * */
    jQuery(document).on('click','.ca_create_product',function(){

        var id = this.id;
        $('.loading').css('display', 'block');
        var data = {
            action: 'ca_create_single_product',
            post_var: id
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $('.loading').css('display', 'none');
            $("#modalContent").html(response);
            $("#notificationModal").modal("show");
            $('#td_'+id).html("Product exists");
        });
        return false;
    });

    /*
    * Update a single product from wordpress to channelAdvisor
    * */
    jQuery(document).on('click','.ca_update_product',function(){
        var sku = this.id;
        $('.loading').css('display', 'block');
        var data = {
            action: 'ca_update_product',
            post_var: sku
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $('.loading').css('display', 'none');
            $("#modalContent").text('Product Update completed!');
            $("#notificationModal").modal("show");
        });
        return false;
    });

    /*
    * Update stock of all products from wordpress to channelAdvisor
    * */
    $("#update_stock").click( function() {
        $('.loading').css('display', 'block');
        var data = {
            action: 'ca_update_stock'
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $('.loading').css('display', 'none');
            $("#modalContent").text('Product stock updated!');
            $("#notificationModal").modal("show");
        });
        return false;
    });

    /*
    * Import orders from channelAdvisor to wordpress
    * */
    $("#import_orders").click( function() {
        $('.loading').css('display', 'block');
        var data = {
            action: 'ca_import_orders'
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $('.loading').css('display', 'none');
            $("#modalContent").text('Orders were imported!');
            $("#notificationModal").modal("show");
        });
        return false;
    });

    /*
    * Update orders status to Channel Advisor from wordpress
    * */
    $("#update_order_status").click( function() {
        $('.loading').css('display', 'block');
        var data = {
            action: 'ca_update_order_status'
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $('.loading').css('display', 'none');
            $("#modalContent").text('Order status were updated!');
            $("#notificationModal").modal("show");
        });
        return false;
    });

    /*
    * Push products from wordpress to channelAdvisor
    * */
    /*$("#push_products").click( function() {
        $(".button").attr('disabled','disabled');
        $("#push_product_loader").css('display','');
        var data = {
            action: 'ca_push_products'
        };
        $.post(the_ajax_script.ajaxurl, data, function(response) {
            $(".button").removeAttr('disabled');
            $("#push_product_loader").css('display','none');
            $("#modalContent").text('Product import completed!');
            $("#notificationModal").modal("show");
        });
        return false;
    });*/
});
