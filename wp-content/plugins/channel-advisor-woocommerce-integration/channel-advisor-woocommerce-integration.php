<?php
/**
 * Plugin Name: ChannelAdvisor Integration
 * Description: ChannelAdvisor Integration to WooCommerce.
 * Version: 1.0
 * Author: ScrumWheel
 */
defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );
define('CA_BASE_PATH', plugin_dir_path(__FILE__));
define('CA_BASE_URL', plugin_dir_url(__FILE__));
define('ChannelAdvisor_BASE_API_URL', 'https://api.channeladvisor.com/');

register_activation_hook( __FILE__, 'ca_plugin_prefix_function_to_run' );
function ca_plugin_prefix_function_to_run(){
    //Add refresh token
    add_option('ca_refresh_token', 'A1Kw4g7KH6IcD4rqcVZ-B4OTdp9QE2JkkxNiAifh9ig');
    //Add Access token
    add_option('ca_access_token', 'tNCAIXBslHZOxvBu-Bf4zEzqM8kU_AdTki5QY7hdggE-32809');
    //Add Access token update time
    add_option('ca_access_token_update_time', date('Y-m-d H:i:s', strtotime('-1 hour')));
    //Add Application Id
    add_option('ca_application_id', 'mkch8yjra8i27r8pprvfr1lvqr19icqy');
    //Add Shared secret
    add_option('ca_shared_secret', 'tyLLCO0GykSa4k-AwHXlgw');
}

register_deactivation_hook( __FILE__, 'ca_plugin_postfix_function_to_run' );
function ca_plugin_postfix_function_to_run(){
    //Delete tokens
    delete_option('ca_refresh_token');
    delete_option('ca_access_token');
    delete_option('ca_access_token_update_time');
    delete_option('ca_application_id');
    delete_option('ca_shared_secret');
}

/**
 * Creates menu for the plugin in the admin section.
 */
add_action('admin_menu', 'ca_register_options_page');
function ca_register_options_page() {
    add_menu_page('CA Integration', 'CA Integration', 'manage_options', 'ca_settings', 'ca_options_page');
    add_submenu_page( 'ca_settings', 'Create Products', 'Create Products', 'manage_options', 'ca_create_product', 'ca_create_product_index');
    add_submenu_page( 'ca_settings', 'Update Products', 'Update Products', 'manage_options', 'ca_update_product', 'ca_update_product_index');
}

/*
 * To refresh the access token if expired
 * */
function ca_refresh_token(){
    try {
        $get_last_update_time = get_option('ca_access_token_update_time');
        $present_time = date('Y-m-d H:i:s');
        if($present_time > date('Y-m-d H:i:s',strtotime($get_last_update_time.'+1 hour'))){
            $response = wp_remote_post( ChannelAdvisor_BASE_API_URL.'oauth2/token', array(
                'body' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => get_option('ca_refresh_token')
                ],
                'headers' => array(
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Basic ' . base64_encode( get_option('ca_application_id') . ':' . get_option('ca_shared_secret') )
                ),
            ));

            if(isset($response['response']) && $response['response']['code'] == 200){
                $result = json_decode($response['body'], true);
                if(isset($result['token_type']) && isset($result['access_token'])){
                    $accessToken = $result['access_token'];
                    update_option('ca_access_token', $accessToken);
                    update_option('ca_access_token_update_time', $present_time);
                }
            } else {
                print ("Access token cannot be updated");
                print_r($response);
                exit;
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Index Page
 * */
function ca_options_page(){
    include CA_BASE_PATH."view/ca-dashboard.php";
}

/*
 * Create product view page
 * */
function ca_create_product_index(){
    try {
        $args = array( 'post_type' => 'product' ,'posts_per_page' => -1);
        $allProducts = new WP_Query( $args );
        $result = $allProducts->posts;
        $products_only_present_in_ca = [];
        $products_common_in_wp_ca = [];
        $wordpressFormattedSku = [];

        foreach ($result as $value){
            $productModel = wc_get_product($value->ID);
            $productData = $productModel->get_data();

            $query = new \WP_Query( array(
                'post_parent' => $value->ID,
                'post_status' => 'publish',
                'post_type' => 'product_variation',
                'posts_per_page' => -1,
            ) );

            //Products need to be created only for the one with variants
            if($query->have_posts()){
                $productColorAttribute = $productModel->get_attribute('pa_color');

                //Create parent product
                if($productData['status'] == 'publish'){
                    if(isset($productColorAttribute)){
                        $data_sku = $productData['sku'].'_'.$productColorAttribute;
                    } else {
                        $data_sku = null;
                    }
                    if(!is_null($data_sku)){
                        //array_push($wordpressFormattedSku, $data_sku);
                        $tempArray = [];
                        $tempArray['Sku'] = $data_sku;
                        $tempArray['Title'] = $productData['name'];
                        $tempArray['WordpressId'] = $value->ID;
                        $wordpressFormattedSku[$data_sku] = $tempArray;
                    }

                    /*while ($query->have_posts()) {
                        $query->next_post();
                        $productVariation = $query->post;
                        $productVariationMeta = get_post_meta($query->post->ID);

                        if(isset($productVariationMeta['attribute_pa_size'])){
                            $childData_sku = $data_sku.'_'.$productVariationMeta['attribute_pa_size'][0];
                        } else {
                            $childData_sku = null;
                        }
                        if(!is_null($childData_sku)){
                            //array_push($wordpressFormattedSku, $childData_sku);
                            $tempArray = [];
                            $tempArray['Sku'] = $childData_sku;
                            $tempArray['Title'] = $productVariation->post_title;
                            $tempArray['WordpressId'] = $query->post->ID;
                            $wordpressFormattedSku[$data_sku] = $tempArray;
                        }
                    }
                    wp_reset_postdata();
                    wp_reset_query();*/
                }
            }
        }

        $channelAdvisorSku = [];
        $getProductUrl = ChannelAdvisor_BASE_API_URL.'v1/Products?$select=ID,Sku,Title,IsParent';
        while(!is_null($getProductUrl)){
            ca_refresh_token();
            $productResponse = wp_remote_get($getProductUrl,[
                'timeout' => 50000,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                )
            ]);
            if(isset($productResponse['body'])){
                $productResponseBody = json_decode($productResponse['body'], true);
                foreach ($productResponseBody['value'] as $value){
                    if($value['IsParent'] == true){
                        $channelAdvisorSku[$value['Sku']] = $value;
                        if(array_key_exists($value['Sku'], $wordpressFormattedSku)){
                            $products_common_in_wp_ca[$value['Sku']] = $wordpressFormattedSku[$value['Sku']];
                        } else {
                            $products_only_present_in_ca[$value['Sku']] = $value;
                        }
                    }
                }
                if(isset($productResponseBody['@odata.nextLink'])){
                    $getProductUrl = $productResponseBody['@odata.nextLink'];
                } else {
                    $getProductUrl = null;
                }
            } else {
                print("error while calling the product API");
                print_r($productResponse);
                exit;
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
    include CA_BASE_PATH."view/ca-create-product.php";
}

/*
 * Update product view page
 * */
function ca_update_product_index(){
    try {
        //Get all the products available in channel advisor
        $channelAdvisorSku = ca_get_channel_advisor_products();
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
    include CA_BASE_PATH."view/ca-update-product.php";
}

/*
 * Import all orders from CA to wordpress
 * */
add_action('wp_ajax_ca_import_orders', 'ca_import_orders');
function ca_import_orders(){
    try {
        $channelAdvisorOrderIds = ca_get_ca_order_id_from_wp();
        $getOrderUrl = ChannelAdvisor_BASE_API_URL.'v1/Orders?exported=false&$expand=Items($expand=Promotions),Fulfillments';
        while(!is_null($getOrderUrl)){
            ca_refresh_token();
            $orderResponse = wp_remote_get($getOrderUrl,[
                'timeout' => 50000,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                )
            ]);

            if(isset($orderResponse['body'])) {
                $orderResponseBody = json_decode($orderResponse['body'], true);

                foreach ($orderResponseBody['value'] as $value){
                    if(!in_array($value['ID'], $channelAdvisorOrderIds)){
                        //Add order to wooCommerce
                        $order = wc_create_order();

                        $billingAddress = array(
                            'first_name' => $value['BillingFirstName'],
                            'last_name'  => $value['BillingLastName'],
                            'company'    => $value['BillingCompanyName'],
                            'email'      => $value['BuyerEmailAddress'],
                            'phone'      => $value['BillingDaytimePhone'],
                            'address_1'  => $value['BillingAddressLine1'],
                            'address_2'  => $value['BillingAddressLine2'],
                            'city'       => $value['BillingCity'],
                            'state'      => $value['BillingStateOrProvince'],
                            'postcode'   => $value['BillingPostalCode'],
                            'country'    => $value['BillingCountry']
                        );
                        $order->set_address( $billingAddress, 'billing' );

                        $shippingAddress = array(
                            'first_name' => $value['ShippingFirstName'],
                            'last_name'  => $value['ShippingLastName'],
                            'company'    => $value['ShippingCompanyName'],
                            'email'      => $value['BuyerEmailAddress'],
                            'phone'      => $value['ShippingDaytimePhone'],
                            'address_1'  => $value['ShippingAddressLine1'],
                            'address_2'  => $value['ShippingAddressLine2'],
                            'city'       => $value['ShippingCity'],
                            'state'      => $value['ShippingStateOrProvince'],
                            'postcode'   => $value['ShippingPostalCode'],
                            'country'    => $value['ShippingCountry']
                        );
                        $order->set_address( $shippingAddress, 'shipping' );
                        $order->set_created_via('Channel Advisor');
                        $order->add_meta_data('ca_order_id', $value['ID']);
                        $order->add_meta_data('from_ca', '1');
                        $order->add_meta_data('ca_order_response', json_encode($value));
                        foreach ($value['Items'] as $item){
                            $productEAN = $item['Sku'];
                            $productId = ca_get_product_from_EAN($productEAN);
                            if(!is_null($productId)){
                                $product = wc_get_product($productId);
                                $order->add_product($product, $item['Quantity']);
                            } else {
                                echo "Product Id not found from EAN number";
                                exit;
                            }
                        }
                        $order->calculate_totals();
                        // Save the data
                        $order->save();

                        ## STOCK and Status update
                        $order->payment_complete();
                    }
                    //Mark the order as exported
                    ca_mark_order_exported($value['ID']);
                }

                if(isset($orderResponseBody['@odata.nextLink'])){
                    $getOrderUrl = $orderResponseBody['@odata.nextLink'];
                } else {
                    $getOrderUrl = null;
                }
            } else {
                print("error while calling the get order API");
                print_r($orderResponse);
                exit;
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Update order status from wooCommerce to Channel Advisor
 * */
add_action('wp_ajax_ca_update_order_status', 'ca_update_order_status');
function ca_update_order_status(){
    try {
        $args = [
            'status' => 'completed',
            'return' => 'ids',
        ];
        $query = new WC_Order_Query( $args );
        $orders = $query->get_orders();
        foreach( $orders as $order_id ) {
            $order = wc_get_order($order_id);
            $caOrderId = $order->get_meta('ca_order_id');

            //Update shipping status at ChannelAdvisor
            $shippingUpdateUrl = ChannelAdvisor_BASE_API_URL.'v1/Orders('.$caOrderId.')/Ship/';
            $shippingUpdateResponse = wp_remote_post($shippingUpdateUrl,[
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                )
            ]);

            if(isset($shippingUpdateResponse['response']) && $shippingUpdateResponse['response']['code'] == 204){
                //echo "Order marked as exported successfully";
            } else {
                print('<pre>');
                print_r($shippingUpdateResponse);
                echo "Order not shipped";
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Mark order as exported
 * */
function ca_mark_order_exported($caOrderId){
    $orderExportUrl = ChannelAdvisor_BASE_API_URL.'v1/Orders('.$caOrderId.')/Export';
    $orderExportResponse = wp_remote_post($orderExportUrl,[
        'headers' => array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . get_option('ca_access_token')
        )
    ]);

    if(isset($orderExportResponse['response']) && $orderExportResponse['response']['code'] == 204){
        //echo "Order marked as exported successfully";
    } else {
        print('<pre>');
        print_r($orderExportResponse);
        echo "Order not marked";
    }
}

/*
 * Get wordpress CA-order-Id
 * */
function ca_get_ca_order_id_from_wp(){
    $caOrderIdArray = [];
    $args = [
        'return' => 'ids',
    ];
    $query = new WC_Order_Query( $args );
    $orders = $query->get_orders();
    foreach( $orders as $order_id ) {
        $order = wc_get_order($order_id);
        $caOrderId = $order->get_meta('ca_order_id');
        array_push($caOrderIdArray, $caOrderId);
    }
    return $caOrderIdArray;
}

/*
 * Get product variation (child product) id
 * from EAN/GTIN number
 * */
function ca_get_product_from_EAN($eanNumber){
    $args = array(
        'post_type' => 'product_variation',
        'meta_query' => array(
            array(
                'key' => 'hwp_var_gtin',
                'value' => $eanNumber,
                'compare' => '=',
            )
        )
    );
    $query = new WP_Query($args);
    if($query->have_posts()){
        $query->the_post();
        $post_id = $query->post->ID;
    } else {
        $post_id = null;
    }
    return $post_id;
}

/*
 * Get all channel advisor products
 * */
function ca_get_channel_advisor_products(){
    try {
        $args = array( 'post_type' => 'product' ,'posts_per_page' => -1);
        $allProducts = new WP_Query( $args );
        $result = $allProducts->posts;
        $wordpressFormattedSku = [];

        foreach ($result as $value){
            $productModel = wc_get_product($value->ID);
            $productData = $productModel->get_data();

            $query = new \WP_Query( array(
                'post_parent' => $value->ID,
                'post_status' => 'publish',
                'post_type' => 'product_variation',
                'posts_per_page' => -1,
            ) );

            //Products need to be created only for the one with variants
            if($query->have_posts()){
                $productColorAttribute = $productModel->get_attribute('pa_color');

                //Create parent product
                if($productData['status'] == 'publish'){
                    if(isset($productColorAttribute)){
                        $data_sku = $productData['sku'].'_'.$productColorAttribute;
                    } else {
                        $data_sku = null;
                    }
                    if(!is_null($data_sku)){
                        $tempArray = [];
                        $tempArray['Sku'] = $data_sku;
                        $tempArray['WordpressId'] = $value->ID;
                        $wordpressFormattedSku[$data_sku] = $tempArray;
                    }
                }
            }
        }

        $channelAdvisorSku = [];
        $getProductUrl = ChannelAdvisor_BASE_API_URL.'v1/Products?$select=ID,Sku,Title,IsParent';
        while(!is_null($getProductUrl)){
            ca_refresh_token();
            $productResponse = wp_remote_get($getProductUrl,[
                'timeout' => 50000,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                )
            ]);
            if(isset($productResponse['body'])){
                $productResponseBody = json_decode($productResponse['body'], true);
                foreach ($productResponseBody['value'] as $value){
                    if($value['IsParent'] == true){

                        /*//Wordpress ID calculation
                        $underscorePosition = strrpos($value['Sku'], '_');
                        $wordpressSKU = substr($value['Sku'], 0, $underscorePosition);
                        $wordpressId = wc_get_product_id_by_sku($wordpressSKU);
                        if(isset($wordpressId)){
                            $value['WordpressId'] = $wordpressId;
                        } else {
                            $value['WordpressId'] = null;
                        }*/
                        if(array_key_exists($value['Sku'], $wordpressFormattedSku)){
                            $value['WordpressId'] = $wordpressFormattedSku[$value['Sku']]['WordpressId'];
                            $channelAdvisorSku[$value['Sku']] = $value;
                        }
                    }
                }
                if(isset($productResponseBody['@odata.nextLink'])){
                    $getProductUrl = $productResponseBody['@odata.nextLink'];
                } else {
                    $getProductUrl = null;
                }
            } else {
                print("error while calling the product API");
                print_r($productResponse);
                exit;
            }
        }
        return $channelAdvisorSku;
    }  catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Create ajax Single product
 * */
add_action('wp_ajax_ca_create_single_product', 'ca_ajax_create_product');
function ca_ajax_create_product(){
    try {
        if(isset($_POST['post_var'])){
            $wordpressId = $_POST['post_var'];
            $summary = create_single_product($wordpressId);
            echo $summary;
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Create ajax Multiple products
 * */
add_action('wp_ajax_ca_create_multiple_products', 'ca_ajax_create_multiple_products');
function ca_ajax_create_multiple_products(){
    try {
        $summary = '';
        if(isset($_POST['productId'])){
            $wordpressIds = $_POST['productId'];
            $idArray = json_decode(stripslashes($wordpressIds));
            foreach ($idArray as $wordpressId){
                $summary .= create_single_product($wordpressId);
            }
        } else {
            $summary .= "No product Id were received";
        }
        echo $summary;
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
    wp_die();
}

/*
 * Create Single product
 * */
function create_single_product($wordpressId){
    try {
        $productModel = wc_get_product($wordpressId);
        $productData = $productModel->get_data();
        $summary = '';

        $query = new \WP_Query( array(
            'post_parent' => $wordpressId,
            'post_status' => 'publish',
            'post_type' => 'product_variation',
            'posts_per_page' => -1,
        ) );

        //Products need to be created only for the one with variants
        if($query->have_posts()){
            $productSizeAttribute = $productModel->get_attribute('pa_size');
            $productColorAttribute = $productModel->get_attribute('pa_color');
            $productSeasonAttribute = $productModel->get_attribute('pa_season');
            $productCollectionAttribute = $productModel->get_attribute('pa_collection');

            //Create parent product
            if($productData['status'] == 'publish'){
                $data = [];
                $data['IsParent'] = true;
                $data['IsInRelationship'] = true;
                $data['RelationshipName'] = 'Size';
                $data['Title'] = $productData['name'];
                $data['Condition'] = 'New';
                $data['Description'] = $productData['description'];
                $data['ShortDescription'] = $productData['short_description'];
                $data['Attributes'] = [];

                if(isset($productSizeAttribute)){
                    $temp = [];
                    $temp['Name'] = 'Size';
                    $temp['Value'] = $productSizeAttribute;
                    array_push($data['Attributes'], $temp);
                }

                if(isset($productColorAttribute)){
                    $temp = [];
                    $temp['Name'] = 'Color';
                    $temp['Value'] = $productColorAttribute;
                    array_push($data['Attributes'], $temp);

                    $data['Sku'] = $productData['sku'].'_'.$productColorAttribute;
                } else {
                    $data['Sku'] = $productData['sku'];
                }

                if(isset($productCollectionAttribute)){
                    $temp = [];
                    $temp['Name'] = 'Collection';
                    $temp['Value'] = $productCollectionAttribute;
                    array_push($data['Attributes'], $temp);
                }

                if(isset($productSeasonAttribute)){
                    $temp = [];
                    $temp['Name'] = 'Season';
                    $temp['Value'] = $productSeasonAttribute;
                    array_push($data['Attributes'], $temp);
                }

                ca_refresh_token();
                $parentProductResponse = wp_remote_post(ChannelAdvisor_BASE_API_URL.'v1/Products',[
                    'body'    => json_encode($data),
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . get_option('ca_access_token')
                    )
                ]);

                if(isset($parentProductResponse['response']) && $parentProductResponse['response']['code'] == 201){
                    //Parent product got created
                    $parentResponseBody = json_decode($parentProductResponse['body'], true);
                    $summary .= '<li>Parent product with sku '.$parentResponseBody['Sku']. ' created</li>';

                    //Add Image to parent product
                    ca_import_images($parentResponseBody['ID'], $wordpressId);

                    //Create Child products
                    ca_create_child_product($wordpressId, $parentResponseBody['ID'], $parentResponseBody['Sku'], $parentResponseBody['Description']);
                    wp_reset_postdata();
                    wp_reset_query();
                }
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        $summary = $e->getTraceAsString();
    }
    return $summary;
}

/*
 * Add/Update images
 * */
function ca_import_images($channelAdvisorId, $wordpressId){
    try {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $productModel = wc_get_product($wordpressId);
        $image_id  = $productModel->get_image_id();
        $image_url = wp_get_attachment_image_url( $image_id, 'full' );

        ca_refresh_token();
        $productResponse = wp_remote_request(ChannelAdvisor_BASE_API_URL."v1/Products(".$channelAdvisorId.")/Images('ITEMIMAGEURL1')?access_token="
            .get_option('ca_access_token'),[
            'body'    => json_encode(['Url'=>$image_url]),
            'method' => 'PUT',
            'headers' => array(
                'Content-Type' => 'application/json',
            )
        ]);
        $productResponseBody = json_decode($productResponse['body'], true);
        if($productResponseBody != ''){
            print("Main Image related issue");
            print_r($productResponse);
        }

        $attachment_ids = $productModel->get_gallery_image_ids();
        $count = 2;
        foreach( $attachment_ids as $attachment_id )
        {
            $gallery_image_url = wp_get_attachment_url( $attachment_id );
            $productResponse = wp_remote_request(ChannelAdvisor_BASE_API_URL."v1/Products(".$channelAdvisorId.")/Images('ITEMIMAGEURL".$count."')?access_token="
                .get_option('ca_access_token'),[
                'body'    => json_encode(['Url'=>$gallery_image_url]),
                'method' => 'PUT',
                'headers' => array(
                    'Content-Type' => 'application/json',
                )
            ]);
            $productResponseBody = json_decode($productResponse['body'], true);
            if($productResponseBody != ''){
                print("Gallery Image related issue");
                print_r($productResponse);
            }
            $count++;
        }

    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Add stock details
 * */
function ca_stock_details($channelAdvisorId, $wordpressId){
    try {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $productModel = wc_get_product($wordpressId);
        $stockQuantity = $productModel->get_stock_quantity();
        
        ca_refresh_token();
        $productResponse = wp_remote_request(ChannelAdvisor_BASE_API_URL."v1/Products(".$channelAdvisorId.")/DCQuantities(0)",[
            'body'    => json_encode(['AvailableQuantity'=>$stockQuantity]),
            'method' => 'PATCH',
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . get_option('ca_access_token')
            )
        ]);
        $productResponseBody = json_decode($productResponse['body'], true);
        if($productResponseBody != ''){
            print("<br>Stock check<br>");
            print_r($productResponse);
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Update all stock
 * */
add_action('wp_ajax_ca_update_stock', 'ca_update_stock');
function ca_update_stock(){
    try {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $args = array( 'post_type' => 'product' ,'posts_per_page' => -1);
        $allProducts = new WP_Query( $args );
        $result = $allProducts->posts;
        foreach ($result as $value) {
            $productModel = wc_get_product($value->ID);
            $productColorAttribute = $productModel->get_attribute('pa_color');
            if(isset($productColorAttribute)){
                $parentProductSku = $productModel->get_sku() . '_'.$productColorAttribute;
            } else {
                $parentProductSku = $productModel->get_sku();
            }

            //Update Child products
            $query = new \WP_Query( array(
                'post_parent' => $value->ID,
                'post_status' => 'publish',
                'post_type' => 'product_variation',
                'posts_per_page' => -1,
            ) );
            if($query->have_posts()){
                while ($query->have_posts()) {
                    $query->next_post();
                    $productVariationMeta = get_post_meta($query->post->ID);

                    if(isset($productVariationMeta['attribute_pa_size'])){
                        $childProductSku = $parentProductSku.'_'.$productVariationMeta['attribute_pa_size'][0];
                    } else {
                        $childProductSku = $parentProductSku;
                    }

                    $childChannelAdvisorID = ca_get_channel_advisor_id($childProductSku);
                    if($childChannelAdvisorID != 0){
                        ca_stock_details($childChannelAdvisorID, $query->post->ID);
                    }
                }
                wp_reset_postdata();
            }
            wp_reset_query();
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Delete product if exists
 * */
function delete_product_if_exists($channelAdvisorID){
    try {
        $response = wp_remote_request(ChannelAdvisor_BASE_API_URL.'v1/Products('.$channelAdvisorID.')',[
            'method' => 'DELETE',
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . get_option('ca_access_token')
            )
        ]);

        if(isset($response['response']) && $response['response']['code'] == 204){
            //echo "Product deleted successfully";
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Delete single product
 * */
add_action('wp_ajax_ca_delete_product', 'ca_ajax_delete_product');
function ca_ajax_delete_product(){
    try {
        if(isset($_POST['post_var'])){
            $sku = $_POST['post_var'];
            $channelAdvisorID = ca_get_channel_advisor_id($sku);
            $getChildProductsURL = ChannelAdvisor_BASE_API_URL.'v1/Products('.$channelAdvisorID.')/Children';

            $response = wp_remote_get($getChildProductsURL,[
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                )
            ]);
            if(isset($response['body'])){
                $responseBody = json_decode($response['body'], true);
                //Delete Child Products
                foreach ($responseBody['value'] as $childIds){
                    delete_product_if_exists($childIds['ChildProductID']);
                }
                //Delete Parent Product
                delete_product_if_exists($channelAdvisorID);
            }
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * Update products
 * */
add_action('wp_ajax_ca_update_product', 'ca_update_product');
function ca_update_product(){
    try {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        print('<pre>');
        if(isset($_POST['post_var'])){
            $formattedSku = $_POST['post_var'];
            $underscorePosition = strrpos($formattedSku, '_');
            $wordpressSKU = substr($formattedSku, 0, $underscorePosition);
            $wordpressId = wc_get_product_id_by_sku($wordpressSKU);

            $summary = '';
            $productModel = wc_get_product($wordpressId);
            $productData = $productModel->get_data();
            $productSizeAttribute = $productModel->get_attribute('pa_size');
            $productColorAttribute = $productModel->get_attribute('pa_color');
            $productSeasonAttribute = $productModel->get_attribute('pa_season');
            $productCollectionAttribute = $productModel->get_attribute('pa_collection');

            //Update parent product
            $data = [];
            $data['Title'] = $productData['name'];
            $data['Description'] = $productData['description'];
            $data['ShortDescription'] = $productData['short_description'];
            $attributeData['Attributes'] = [];

            if(isset($productSizeAttribute)){
                $temp = [];
                $temp['Name'] = 'Size';
                $temp['Value'] = $productSizeAttribute;
                array_push($attributeData['Attributes'], $temp);
            }

            if(isset($productColorAttribute)){
                $temp = [];
                $temp['Name'] = 'Color';
                $temp['Value'] = $productColorAttribute;
                $parentProductSku = $productModel->get_sku() . '_'.$productColorAttribute;
                array_push($attributeData['Attributes'], $temp);
            } else {
                $parentProductSku = $productModel->get_sku();
            }

            if(isset($productCollectionAttribute)){
                $temp = [];
                $temp['Name'] = 'Collection';
                $temp['Value'] = $productCollectionAttribute;
                array_push($attributeData['Attributes'], $temp);
            }

            if(isset($productSeasonAttribute)){
                $temp = [];
                $temp['Name'] = 'Season';
                $temp['Value'] = $productSeasonAttribute;
                array_push($attributeData['Attributes'], $temp);
            }

            $channelAdvisorID = ca_get_channel_advisor_id($parentProductSku);
            if($channelAdvisorID != 0){
                //https://api.channeladvisor.com/v1/Products(420988)
                ca_refresh_token();
                $parentProductResponse = wp_remote_request(ChannelAdvisor_BASE_API_URL.'v1/Products('.$channelAdvisorID.')',[
                    'body'    => json_encode($data),
                    'method' => 'PUT',
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . get_option('ca_access_token')
                    )
                ]);

                if(isset($parentProductResponse['response']) && $parentProductResponse['response']['code'] == 204){
                    $summary .= '<li>Parent product with sku '.$parentProductSku. ' got updated</li>';

                    //Add/Update Image to parent product
                    ca_import_images($channelAdvisorID, $productData['id']);

                    //Create Child products after deleting them
                    $summary .= ca_create_child_product($productData['id'], $channelAdvisorID, $parentProductSku, $productData['description']);
                    /*$query = new \WP_Query( array(
                        'post_parent' => $productData['id'],
                        'post_status' => 'publish',
                        'post_type' => 'product_variation',
                        'posts_per_page' => -1,
                    ) );
                    if($query->have_posts()){
                        while ($query->have_posts()) {
                            $query->next_post();
                            $productVariation = $query->post;
                            $productVariationMeta = get_post_meta($query->post->ID);
                            $childData = [];
                            $childData['Title'] = $productVariation->post_title;
                            $childData['Condition'] = 'New';
                            $childData['Description'] = $productData['description'];
                            //$childData['ShortDescription'] = $productData['short_description'];
                            $childData['EAN'] = $productVariationMeta['hwp_var_gtin'][0];
                            $childData['RetailPrice'] = (float)$productVariationMeta['_regular_price'][0];
                            $childAttributeData['Attributes'] = [];

                            if(isset($productColorAttribute)){
                                $temp = [];
                                $temp['Name'] = 'Color';
                                $temp['Value'] = $productColorAttribute;
                                array_push($childAttributeData['Attributes'], $temp);
                            }

                            if(isset($productVariationMeta['attribute_pa_size'])){
                                $temp = [];
                                $temp['Name'] = 'Size';
                                $temp['Value'] = $productVariationMeta['attribute_pa_size'][0];
                                array_push($childAttributeData['Attributes'], $temp);
                                $childProductSku = $parentProductSku.'_'.$productVariationMeta['attribute_pa_size'][0];
                            } else {
                                $childProductSku = $parentProductSku;
                            }

                            $childChannelAdvisorID = ca_get_channel_advisor_id($childProductSku);

                            if($childChannelAdvisorID != 0){
                                delete_product_if_exists($childChannelAdvisorID);
                                ca_refresh_token();
                                $childProductResponse = wp_remote_request(ChannelAdvisor_BASE_API_URL.'v1/Products('.$childChannelAdvisorID.')',[
                                    'body'    => json_encode($childData),
                                    'method' => 'PUT',
                                    'headers' => array(
                                        'Content-Type' => 'application/json',
                                        'Authorization' => 'Bearer ' . get_option('ca_access_token')
                                    )
                                ]);

                                if(isset($childProductResponse['response']) && $childProductResponse['response']['code'] == 204){
                                    $summary .= 'Child product with sku '.$childProductSku. ' got updated <br>';

                                    //Add parent images to child product
                                    ca_import_images($childChannelAdvisorID, $productData['id']);

                                    //Update stock
                                    ca_stock_details($childChannelAdvisorID, $query->post->ID);

                                } else {
                                    print_r($childProductResponse);
                                    print_r($childData);
                                }
                            }
                        }
                        wp_reset_postdata();
                    }
                    wp_reset_query();*/
                } else {
                    print_r($parentProductResponse);
                }
            }
        } else {
            echo "Product Id not found";
        }
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/*
 * create child products and
 * delete if already exists
 * */
function ca_create_child_product($parentProductWordpressId, $parentCAId, $parentSku, $parentDescription){
    $summary = '';
    $query = new \WP_Query( array(
        'post_parent' => $parentProductWordpressId,
        'post_status' => 'publish',
        'post_type' => 'product_variation',
        'posts_per_page' => -1,
    ) );
    while ($query->have_posts()) {
        $query->next_post();
        $productVariation = $query->post;
        $productVariationMeta = get_post_meta($query->post->ID);
        $childData = [];
        $childData['IsParent'] = "false";
        $childData['IsInRelationship'] = "true";
        $childData['ParentProductID'] = $parentCAId;
        $childData['RelationshipName'] = 'Size';
        $childData['Title'] = $productVariation->post_title;
        $childData['Condition'] = 'New';
        $childData['Description'] = $parentDescription;
        $childData['EAN'] = $productVariationMeta['hwp_var_gtin'][0];
        $childData['Sku'] = $productVariationMeta['hwp_var_gtin'][0];
        $childData['RetailPrice'] = (float)$productVariationMeta['_regular_price'][0];
        $childData['Attributes'] = [];

        if(isset($productColorAttribute)){
            $temp = [];
            $temp['Name'] = 'Color';
            $temp['Value'] = $productColorAttribute;
            array_push($childData['Attributes'], $temp);
        }

        if(isset($productVariationMeta['attribute_pa_size'])){
            $temp = [];
            $temp['Name'] = 'Size';
            $temp['Value'] = $productVariationMeta['attribute_pa_size'][0];
            array_push($childData['Attributes'], $temp);
        }

        $channelAdvisorID = ca_get_channel_advisor_id($childData['Sku']);
        delete_product_if_exists($channelAdvisorID);

        ca_refresh_token();
        $childProductResponse = wp_remote_post(ChannelAdvisor_BASE_API_URL.'v1/Products',[
            'body'    => json_encode($childData),
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . get_option('ca_access_token')
            )
        ]);

        if(isset($childProductResponse['response']) && $childProductResponse['response']['code'] == 201){
            $childResponseBody = json_decode($childProductResponse['body'], true);
            $summary .= '<li>Child product with sku '.$childResponseBody['Sku']. ' created </li>';

            //Add parent images to child product
            ca_import_images($childResponseBody['ID'], $parentProductWordpressId);

            //Update stock
            ca_stock_details($childResponseBody['ID'], $query->post->ID);

        } else {
            print_r($childProductResponse);
            print_r($childData);
        }
    }
    wp_reset_postdata();
    wp_reset_query();
    return $summary;
}

/*
 * Get Channel Advisor Id from wordpress SKU
 * */
function ca_get_channel_advisor_id($sku){
    try {
        ca_refresh_token();
        //https://api.channeladvisor.com/v1/Products?$filter=Sku eq 'BGM-92-SW83'&$select=ID
        $url = ChannelAdvisor_BASE_API_URL.'v1/Products?$filter=Sku eq \''.$sku.'\'&$select=ID';
        $productResponse = wp_remote_get($url,[
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . get_option('ca_access_token')
            )
        ]);
        $productResponseBody = json_decode($productResponse['body'], true);
        if(isset($productResponseBody['value'][0]['ID']))
            return $productResponseBody['value'][0]['ID'];
        /*else {
            echo "Product not found with sku: ".$sku."<br>";
        }*/
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}

/**
 * Attaches necessary js & css files
 */
add_action( 'admin_enqueue_scripts', 'load_custom_ca_scripts' );
function load_custom_ca_scripts(){
    wp_enqueue_style( 'bootstrap', plugins_url('assets/css/bootstrap.min.css', __FILE__) );
    wp_enqueue_style( 'dataTables.bootstrap', plugins_url('assets/css/dataTables.bootstrap.min.css', __FILE__) );
    wp_enqueue_style( 'ca-custom-css', plugins_url('assets/css/custom.css', __FILE__) );

    wp_enqueue_script( 'bootstrap', plugins_url( 'assets/js/bootstrap.min.js', __FILE__ ) );
    wp_enqueue_script( 'dataTables', plugins_url( 'assets/js/jquery.dataTables.min.js', __FILE__ ) );
    wp_enqueue_script( 'dataTable-bootstrap', plugins_url( 'assets/js/dataTables.bootstrap.min.js', __FILE__ ) );
    wp_enqueue_script( 'ca-custom', plugins_url( 'assets/js/ca_custom.js', __FILE__ ) );
}
/*
 * function import all products at once
 * */
/*add_action('wp_ajax_ca_push_products', 'ca_import_all_products');
function ca_import_all_products(){
    try {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        print('<prE>');
        $summary = '';
        $args = array( 'post_type' => 'product' ,'posts_per_page' => -1);
        $allProducts = new WP_Query( $args );
        $result = $allProducts->posts;
        foreach ($result as $value){
            $productModel = wc_get_product($value->ID);
            $productData = $productModel->get_data();

            $query = new \WP_Query( array(
                'post_parent' => $value->ID,
                'post_status' => 'publish',
                'post_type' => 'product_variation',
                'posts_per_page' => -1,
            ) );

            //Products need to be created only for the one with variants
            if($query->have_posts()){
                $productSizeAttribute = $productModel->get_attribute('pa_size');
                $productColorAttribute = $productModel->get_attribute('pa_color');
                $productSeasonAttribute = $productModel->get_attribute('pa_season');
                $productCollectionAttribute = $productModel->get_attribute('pa_collection');

                //Create parent product
                if($productData['status'] == 'publish'){
                    $data = [];
                    $data['IsParent'] = true;
                    $data['IsInRelationship'] = true;
                    $data['RelationshipName'] = 'Size';
                    $data['Title'] = $productData['name'];
                    $data['Condition'] = 'New';
                    $data['Description'] = $productData['description'];
                    $data['ShortDescription'] = $productData['short_description'];
                    $data['Attributes'] = [];

                    if(isset($productSizeAttribute)){
                        $temp = [];
                        $temp['Name'] = 'Size';
                        $temp['Value'] = $productSizeAttribute;
                        array_push($data['Attributes'], $temp);
                    }

                    if(isset($productColorAttribute)){
                        $temp = [];
                        $temp['Name'] = 'Color';
                        $temp['Value'] = $productColorAttribute;
                        array_push($data['Attributes'], $temp);

                        $data['Sku'] = $productData['sku'].'_'.$productColorAttribute;
                    } else {
                        $data['Sku'] = $productData['sku'];
                    }

                    if(isset($productCollectionAttribute)){
                        $temp = [];
                        $temp['Name'] = 'Collection';
                        $temp['Value'] = $productCollectionAttribute;
                        array_push($data['Attributes'], $temp);
                    }

                    if(isset($productSeasonAttribute)){
                        $temp = [];
                        $temp['Name'] = 'Season';
                        $temp['Value'] = $productSeasonAttribute;
                        array_push($data['Attributes'], $temp);
                    }

                    ca_refresh_token();
                    $parentProductResponse = wp_remote_post(ChannelAdvisor_BASE_API_URL.'v1/Products',[
                        'body'    => json_encode($data),
                        'headers' => array(
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . get_option('ca_access_token')
                        )
                    ]);

                    if(isset($parentProductResponse['response']) && $parentProductResponse['response']['code'] == 201){
                        //Parent product got created
                        $parentResponseBody = json_decode($parentProductResponse['body'], true);
                        $summary .= 'Parent product with sku '.$parentResponseBody['Sku']. ' got created with parent id '.$parentResponseBody['ID'].'<br>';

                        //Add Image to parent product
                        ca_import_images($parentResponseBody['ID'], $value->ID);

                        //Create Child products
                        while ($query->have_posts()) {
                            $query->next_post();
                            $productVariation = $query->post;
                            $productVariationMeta = get_post_meta($query->post->ID);
                            $childData = [];
                            $childData['IsParent'] = "false";
                            $childData['IsInRelationship'] = "true";
                            $childData['ParentProductID'] = $parentResponseBody['ID'];
                            $childData['RelationshipName'] = 'Size';
                            $childData['Title'] = $productVariation->post_title;
                            $childData['Condition'] = 'New';
                            $childData['Description'] = $parentResponseBody['Description'];
                            $childData['EAN'] = $productVariationMeta['hwp_var_gtin'][0];
                            $childData['RetailPrice'] = (float)$productVariationMeta['_regular_price'][0];
                            $childData['Attributes'] = [];

                            if(isset($productColorAttribute)){
                                $temp = [];
                                $temp['Name'] = 'Color';
                                $temp['Value'] = $productColorAttribute;
                                array_push($childData['Attributes'], $temp);
                            }

                            if(isset($productVariationMeta['attribute_pa_size'])){
                                $temp = [];
                                $temp['Name'] = 'Size';
                                $temp['Value'] = $productVariationMeta['attribute_pa_size'][0];
                                array_push($childData['Attributes'], $temp);
                                $childData['Sku'] = $parentResponseBody['Sku'].'_'.$productVariationMeta['attribute_pa_size'][0];
                            } else {
                                $childData['Sku'] = $parentResponseBody['Sku'];
                            }

                            ca_refresh_token();
                            $childProductResponse = wp_remote_post(ChannelAdvisor_BASE_API_URL.'v1/Products',[
                                'body'    => json_encode($childData),
                                'headers' => array(
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer ' . get_option('ca_access_token')
                                )
                            ]);

                            if(isset($childProductResponse['response']) && $childProductResponse['response']['code'] == 201){
                                $childResponseBody = json_decode($childProductResponse['body'], true);
                                $summary .= 'Child product with sku '.$childResponseBody['Sku']. ' got created <br>';

                                //Add parent images to child product
                                ca_import_images($childResponseBody['ID'], $value->ID);

                                //Update stock
                                ca_stock_details($childResponseBody['ID'], $query->post->ID);

                            } else {
                                print_r($childProductResponse);
                                print_r($childData);
                            }
                        }
                        wp_reset_postdata();
                        wp_reset_query();
                    }
                }
            }
        }
        print $summary;
    } catch (Exception $e){
        echo $e->getMessage();
        echo $e->getTraceAsString();
    }
}*/
?>